#include "stack.h"
#include <assert.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#define T Stack_T

struct T {
    int count;
    char ID[6];
    struct elem {
        void *x;
        struct elem *link;
    } *head;
};

T Stack_new(void) {
    T stk;

    stk = (T) malloc(sizeof(struct T));
    stk->count = 0;
    stk->head = NULL;
    strcpy(stk->ID, "stack");
    return stk;
}

int Stack_empty(T stk) {
    /* Checked runtime error implementation.
     * if stk is nonzero it does nothing but
     * halts program execution otherwise. */
    assert(stk);
    /* Checks if stack was created with Stack_new() */
    assert(strcmp(stk->ID, "stack") == 0);
    return stk->count == 0;
}

void Stack_push(T stk, void *x) {
    struct elem *t;

    assert(stk);
    assert(strcmp(stk->ID, "stack") == 0);
    t = (struct elem*) malloc(sizeof(struct elem));
    t->x = x;
    t->link = stk->head;
    stk->head = t;
    stk->count++;
}

void *Stack_pop(T stk) {
    void *x;
    struct elem *t;

    assert(stk);
    assert(strcmp(stk->ID, "stack") == 0);
    assert(stk->count > 0);
    t = stk->head;
    stk->head = t->link;
    stk->count--;
    x = t->x;
    free(t);
    t = NULL;
    return x;
}

void Stack_free(T *stk) {
    struct elem *t, *u;

    assert(stk && *stk);
    assert(strcmp( (*stk)->ID, "stack") == 0);
    for (t = (*stk)->head; t; t = u) {
        u = t->link;
        free(t);
        t = NULL;
    }
    free(*stk);
    *stk = NULL;
}