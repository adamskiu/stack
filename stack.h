#ifndef STACK_INCLUDED
#define STACK_INCLUDED

#define T Stack_T
typedef struct T *T;

Stack_T Stack_new  (void);
int     Stack_empty(T  stk);
void    Stack_push (T  stk, void *x);
void   *Stack_pop  (T  stk);
void    Stack_free (T *stk);

#undef T
#endif

/***
 *
 * Checked runtime errors:
 * 1. Passing NULL Stack_T to any routine in this interface;
 * 2. passing a null pointer to a Stack_T to Stack_free;
 * 3. passing an empty stack to Stack_pop;
 * 4. passing a stack which was not manufactured by Stack_new to
 *    any function from this interface/
 *
 * Exceptions:
 * 1. Stack_new and Stack_push can raise Memory allocation error.
 */

/*
 * Checked and uncheck runtime errors are not expected
 * user errors, such as failing to open a file.
 *
 * Runtime errors are breaches of the contract between clients
 * and implementations, and are program bugs from which there
 * is no recovery.
 *
 * An unchecked runtime error is a breach of contract that
 * implementations do not guarantee to detect.
 * If an unchecked runtime error occurs, execution may continue,
 * but with unpredictable and perhaps unrepeatable results.
 *
 * Good interfaces avoid unchecked runtime errors when possible,
 * but must specify those that occur.
 *
 * For example an arithmetic interface must specify that division
 * by zero is an unchecked runtime error. It could check for
 * division by zero, but leaves is as an unchecked runtime error
 * so that its function mimic the behavior of C's built-in
 * division operators, whose behavior is undefined.
 * Making division by zero a checked runtime error is a reasonable
 * alternative.
 *
 * A checked runtime error is a breach of contract that
 * implementations guarantee to detect.
 * These errors announce a client's failure to adhere
 * to its part of the contract; it's the client
 * responsibility to avoid them. The Stack interface
 * specifies three checked runtime errors.
 *
 *
 * Exceptions are conditions that, while possible, rarely occur.
 * Programs may be able to recover from exceptions.
 * Running out of memory is an example.
 *
 * Clients can handle exceptions and take corrective action.
 * An unhandled exception is treated as a checked runtime error.
 *
 * */