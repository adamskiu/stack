#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

int main() {
    int *value = 0;
    Stack_T stk  = Stack_new();

    for (int i  = 1; i <= 10; i++) {
        value = malloc(sizeof(value));
        *value = i;
        Stack_push(stk, value);
    }

    while(!Stack_empty(stk)) {
        value = Stack_pop(stk);
        printf("%d\n", *value);
        free(value);
    }

    Stack_free(&stk);

    return 0;
}